Vue.component('G2G_g2', {
    template:
'   <div data-tpl="G2G_g2" :class="root">' +
'     <div :class="body">' +
'        <p>此系統為:{{info.sysName}}</p>' +
'        <form class="form-horizontal" id="login">'+
'            <div class="form-group has-success has-feedback">'+
'                <label class="col-sm-2" for="exampleInputName2">Name</label>'+
'                <div class="col-sm-8">'+
'                    <input class="form-control" type="text" name="name" v-model=info.name>'+
'                    <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>'+
'                    <span id="inputSuccess2Status" class="sr-only">(success)</span>'+
'                </div>'+
'            </div>'+
'            <div class="form-group">'+
'                <div class="col-sm-offset-5">'+
'                  <button type="submit" class="btn btn-primary" @click=submit>Sign in</button>'+
'                </div>'+
'            </div>'+
'        </form>'+
'     </div>' +
'   </div>',
    props: ['info'],
    data: function() {
        return {
            //Style
            root: 'FunA-style',
            body: 'container-fluid',
            in_text: '',
            btn: ''
        }
    },
    methods: {
        'submit': function() {
            $("#login").submit(function(event) {
              console.log($(this).serializeArray());
              event.preventDefault();
            });
        }
    }
})
