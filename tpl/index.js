Vue.component('index', {
    template:
        '<div data-tpl="index">' +
        '    <navbar :child.sync=child></navbar>' +
        '    <sidebar :child.sync=child></sidebar>' +
        '    <div id="page-wrapper">' +
        '       <div :class="container">' +
        '           <component :is="child" ' +
        '                       transition="fade" ' +
        '                       transition-mode="out-in">' +
        '           </compnent>' +
        '       </div>' +
        '    </div>' +
        '</div>',
    data: function() {
        return {
            //Data
            child: 'G2G_demo',
            //Style
            container: 'main-area'
        }
    },
    props: ['msg']
})
