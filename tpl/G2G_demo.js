Vue.component('G2G_demo', {
    template:
    '<div data-tpl="G2G_demo" :class=root>' +
    '  <form class="form-horizontal">'+
    '      <ul class="nav nav-tabs">' +
    '        <label class="checkbox-inline" v-for="(index, item) in obj.checkList">' +
    '          <input type="checkbox" value={{item.name}}' +
    '                 v-model=switchCheck>{{item.name}}</label>' +
    '      </ul>' +
    '  </form>' +
    '  <div>' +
    '  <!-- Nav tabs -->' +
    '  <ul class="nav nav-tabs" role="tablist">' +
    '    <li v-for="item in obj.checkList" v-if=item.checked ' +
    '        :class="getActiveStyle(item.name)" ' +
    '        role="presentation">' +
    '      <a href="#" data-toggle="tab" ' +
    '         @click="switchPage">{{item.name}}</a>' +
    '    </li>' +
    '  </ul>' +
    '  <div class="tab-content">' +
    '    <component :is="funIndex" ' +
    '               transition="fade" ' +
    '               transition-mode="out-in" ' +
    '               :info="dataObj">' +
    '     </compnent>' +
    '  </div>' +
    '  </div>' +
    '</div>',
    props: ['outerPlatforms'],
    data: function() {
        return {
            //All data
            obj: {
                activeTag: '',//Current active compnent.
                //Generator checkbox's number from server site.
                checkList: [
                    {id: 'g1', name: '非追不可', checked: false, active: false},
                    {id: 'g2', name: '非登不可', checked: false, active: false},
                    {id: 'g3', name: '邊境控管', checked: false, active: false},
                    {id: 'g4', name: '水產驗證', checked: false, active: false},
                    {id: 'g5', name: '原料驗證', checked: false, active: false}
                ]
            },
            dataObj: {
                sysName: '',
                name: 'My name',
                nickName: '',
                tel: '',
                address: '',
                uniqueId: ''
            },
            //Child index
            funIndex: '',
        }
    },
    methods: {
        switchPage: function() {
            this.switchActive = event.target.innerText;
        },//End of switchPage().
        getActiveStyle: function(item) {
            //According to array's item to set tab style(active or null).
            if (this.obj.activeTag === item) return 'active';
            else return '';
        }//End of getActiveStyle().
    },
    computed: {
        switchCheck: {
            get: function() {
                return;
            },
            set: function(val) {
                var checks = 0;
                var tagName = event.target.value;
                var originIndex = 0;
                for (row in this.obj.checkList) {
                    //If name equivalent checkList's name.
                    if (this.obj.checkList[row].name === tagName) {
                        originIndex = row;
                        this.obj.checkList[row].checked = val;
                    }//fi
                    if (this.obj.checkList[row].checked) checks += 1;
                }//endfor
                if (val) {
                    if (checks === 1) {
                        this.switchActive = tagName;
                    }//fi
                } else {
                    var flag = true;
                    if (this.switchActive === tagName) {
                        for (var i = originIndex; i > -1; i--) {
                            if (this.obj.checkList[i].checked) {
                                this.switchActive = this.obj.checkList[i].name;
                                flag = false;
                                break;
                            }
                        }//endfor
                        if (flag) {
                            for (var i = originIndex; i < this.obj.checkList.length; i++) {
                                if (this.obj.checkList[i].checked) {
                                    this.switchActive = this.obj.checkList[i].name;
                                    flag = false;
                                    break;
                                }
                            }//endfor
                        }//fi
                        if (flag) {
                            this.switchActive = '';
                        }//fi
                    }//fi
                }//fi
            }
        },
        switchActive: {
            get: function() {
                //console.log(this.obj.activeTag);
                return this.obj.activeTag;
            },
            set: function(val) {
                if (val === '') {
                    this.funIndex = '';
                } else {
                    //Seach id from checkList object.
                    for (row in this.obj.checkList) {
                        //If name equivalent checkList's name.
                        if (this.obj.checkList[row].name === val) {
                            this.funIndex = 'G2G_' + this.obj.checkList[row].id;
                        }//fi
                    }//endfor
                }//fi
                //Set actice name to child dataObj and current component tab name.
                this.dataObj.sysName = val;
                this.obj.activeTag = val;
            }
        }//End of switchActive.
    }
});
