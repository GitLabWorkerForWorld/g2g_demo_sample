#Build G2G_demo function for boss meeting.

Requirement :
  1. vue.js
  2. jquery
  3. bootstarp
  4. metisMenu
  5. font-awesome
  6. sb-adminv2(from:https://startbootstrap.com/template-overviews/sb-admin/)

本專案為B2B HUB開發前測試框架，包含導引列和左側邊瀏覽列(功能)。
原始碼實現各個頁籤切換的效果以及組件的轉換，主要目的為方便呈現和選擇。

實際可用的原始碼包含一個主要分支"master"以及三個標籤。
1. v0.0.1：展示無序的tab頁籤，實現方式是JS原生函式"Object.defineProperty"。
2. v0.0.2：展示無序的tab頁籤，實現方式是用vue.js提供的方法"vue.watch"。
3. v0.0.3：展示有序的tab頁籤，實現方式用vue.js本身的資料操作，包含陣列的操作及搜尋。
