window.onload = function() {
    function createSrc(path) {
        var script = document.createElement('script');
        script.src = path;
        bodyHtml.appendChild(script);
    }//End fo createSrc().
    var jsLib = [
        "index", "navbar", "sidebar",
        "G2G_demo", "G2G_g1", "G2G_g2", "G2G_g3", "G2G_g4", "G2G_g5",
        "item1",
        "item2"
    ];
    var bodyHtml = document.getElementsByTagName('body')[0];
    for(var i = 0; i < jsLib.length; i++) {
        createSrc("./tpl/" + jsLib[i] + ".js");
    }//endfor
    //Add main.js
    createSrc("./js/main.js");
}//End of window.onload().
